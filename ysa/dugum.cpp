
#include "dugum.hpp"

using namespace std;

namespace yz{

	dugum::dugum(int giris_adet,DUGUM_ISLEV islev){
		this->islev_tipi = islev;
		this->giris_adet = giris_adet;
		this->giris_deger.resize(giris_adet);
		this->agirliklar.assign(giris_adet,0.001);
	}

	dugum::~dugum(){


	}

	float dugum::islev(float deger){
		if(this->islev_tipi == SIGMOID){
			cout<<"Sigmoid"<<endl;
    		return (1/(1+exp(-1*deger))); // Sigmoid 
		}
		else if(this->islev_tipi == RELU){
			cout<<"Relu"<<endl;
			if(deger<=0) return 0;  else return 1; // RELU
		}
		else if(this->islev_tipi == LRELU){
			cout<<"Leakly Relu"<<endl;
			if(deger<0) return 0.001;  else return deger; // Leakly RELU
		}
		else{
			cout<<"Aktivasyon fonksiyonu seçilemedi."<<endl;
			exit(1);
		}
	}

	float dugum::turev(float deger){

		if(this->islev_tipi == SIGMOID){
			cout<<"Sigmoid"<<endl;
    		return 	deger * (1- deger); // Sigmoid 
		}
		else if(this->islev_tipi == RELU){
			cout<<"Relu"<<endl;
			return 0; // RELU
		}
		else if(this->islev_tipi == LRELU){
			cout<<"Leakly Relu"<<endl;
			if(deger<0) return 0;  else return 1; // Leakly RELU
		}
		else{
			cout<<"Aktivasyon fonksiyonu seçilemedi."<<endl;
			exit(1);
		}
	}

	float dugum::cikis_hatasi(float hedef){
		return  -1*(hedef - this->sonuc);
	}

	float dugum::dugum_hatasi(float hata,float agirlik){
		return hata * agirlik;
	}

	float dugum::yeni_agirlik(float deger,float agirlik,float hata){
		return agirlik - (0.5 * hata * deger);
	}

	float dugum::topla(){
		float s=0;	
		for(int j=0;j<this->giris_adet;j++){
			s = s+(this->giris_deger[j] * this->agirliklar[j]);
		}
		return s;
	}

	float dugum::hesapla(vector<float> giris_deger){
		this->giris_deger = giris_deger;
		cout<<"Giriş ";
		for(int i=0;i<giris_adet;i++)
			cout<<giris_deger[i]<<"|";
		this->toplam = this->topla();
		cout<<"Toplam "<<this->toplam<<endl;
		this->sonuc = this->islev(toplam);
		cout<<"Çıkış "<<this->sonuc<<endl;
	 	return this->sonuc;
	}

	float dugum::hesapla(float giris_deger){
		cout<<"Giriş "<<giris_deger<<endl;
	 	this->sonuc =  this->islev(giris_deger);
		cout<<"Çıkış "<<this->sonuc<<endl;
	 	return this->sonuc;
	}

	void dugum::egit(){

	}

}


