#ifndef		DUGUM_HPP
#define		DUGUM_HPP

#include <vector>
#include <math.h>
#include <iostream>

using namespace std;

namespace yz{

	enum DUGUM_ISLEV{SIGMOID,RELU,LRELU};

	class dugum{

	public:
		dugum(DUGUM_ISLEV);
		~dugum();
		float			hesapla(vector<float>);
		float			hesapla(float);
		float			islev(float);
		float			topla();
		vector<float>	giris_deger;
		vector<float>	agirliklar;
		int				giris_adet;
		DUGUM_ISLEV		islev_tipi;

	};


}


#endif