#ifndef		YSA_HPP
#define		YSA_HPP

#include <iostream>
#include <stdlib.h>
#include "dugum.hpp"
#include <vector>
#include "katman.hpp"


using namespace std;


namespace yz{

	
	class ysa{

	public:	
		ysa(vector<int>,DUGUM_ISLEV);					//ag({3,54,75,56},DUGUM_ISLEV.SIGMOID);
		~ysa();
		vector<float>						giris_deger;
		vector<float>						cikis_deger;
		vector<katman*>						katmanlar; 
		vector<int>							katman_yapisi;
		vector<float>						calistir(vector<float>);

	};


}


#endif