#ifndef		KATMAN_HPP
#define 	KATMAN_HPP

#include <vector>
#include "dugum.hpp"

using namespace std;

namespace yz{


	class katman{

	public:
		vector<dugum*>	dugumler;
		katman(int,DUGUM_ISLEV);
		~katman();
		vector<float>	hesapla(vector<float>);
		vector<float>	giris_deger;
		vector<float>	cikis_deger;
	
	};


}


#endif